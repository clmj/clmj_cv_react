import React, { Fragment, Component } from 'react';
import Speakers from './Speakers.js'
import './App.css';
import './csshake-slow.css'

class App extends Component {
    state = {
        tabs: ['Mon Profil', 'Mes Compétences', 'Me Contacter'],
        tabIndex: 0,
        slideIndex: 0
    }

    createTabs = () => {
        let tabs = []
        for (let i = 0; i < this.state.tabs.length; i++) {
            tabs.push(<Tab name={this.state.tabs[i]} index={i} isActive={ this.state.tabIndex===i } onClick={ this.changeTab }></Tab>)
        }
        return tabs
    }

    changeTab = (index) => {
        this.setState({ tabIndex: index })
        this.setState({ slideIndex: 0 })
    }

    changeSlide = (direction) => {
        let currentSlide = this.state.slideIndex
        currentSlide += direction
        this.setState({ slideIndex : currentSlide })
    }

    render () {
        const slidesContent = [
        
            [`Bonjour, je suis Clément Jouffre et j'ai 26 ans.<br />
            Je suis <h1>développeur web et web mobile.</h1><br />
            Je suis créatif, enthousiaste, curieux et débrouillard.<br />
            Je possède un bon niveau d'anglais oral et écrit.<br />
            J'aime l'écriture, le cinéma, la bande dessinée, le jeu vidéo, Austin Powers et Al Pacino...`, 
        
            `En 2015 j'ai concrétisé mon goût pour l'<h2>illustration en créant une micro-entreprise d'infographie.</h2>
            <br />J'ai conçu dans ce cadre des supports de communication Print.
            <br /><a href="./Portfolio/" class="font">Vous trouverez ici un aperçu de mes réalisations</a>`,
        
            `J'ai intégré en 2018 la formation Simplon.Ve au Cheylard (07).
            <br />J'y apprend une méthodologie de travail (Agile, SCRUM, Kanban), y étend ma culture de <h2>l'informatique et du web, 
            et y acquiert des compétences techniques</h2>.`],
        
            [`Mon IDE de prédilection est Visual Studio Code.
            <br />Je sais mettre en place un serveur local type Wamp ou un environnement NodeJs.
            <br />Utiliser un outil de gestion de version (type GIT)
            <br />Installer et utiliser des systèmes de gestion de contenus (notamment Wordpress).
            <br />J'utilise également RAD Studio pour développer des applications natives en Delphi.`, 
        
            `J'utilise <h2>HTML5 et CSS3 pour créer des sites Responsive ou Adaptive en utilisant Flexbox ou Bootstrap.
            <br />Javascript et Vue.JS sont mes alliés dans la manipulation du DOM et la conception d'interfaces riches</h2>.`,
        
            `Afin de rendre <h2>dynamiques mes projets web j'utilise aussi bien PHP et le framework Symfony que l'environnement NodeJS.
            <br />Je sais traiter et manipuler des données via le sgbd MySQL ou le format JSON</h2>.`],
        
            [`<div>
                <span>Par mail</span>
                <h3>clement.jouffre<span style="display: none">null</span>@yahoo.fr</h3>
            </div>`]
        ]

        return (
            <Fragment>
            <div className="top">
                <nav>
                    {this.createTabs()}
                </nav>
            
                <main id="bubble" className={ 'bubble'+this.state.tabIndex }>
                    <div className="bubbleContent">

                        <SlideBtn
                        direction={-1}
                        isActive={ this.state.slideIndex !== 0 && this.state.tabIndex !== 2}
                        onClick = { this.changeSlide }>
                        </SlideBtn>

                        <Slide 
                        text={slidesContent[this.state.tabIndex][this.state.slideIndex]}>
                        </Slide>

                        <SlideBtn
                        direction={1}
                        isActive={ this.state.slideIndex !== 2 && this.state.tabIndex !== 2 }
                        onClick = { this.changeSlide}>
                        </SlideBtn>

                    </div>
                </main>
                <div id="tick" className={'tick tick'+this.state.tabIndex}></div>
            </div>
            <div id="bois">
                <Speakers activeTab={ this.state.tabIndex } ></Speakers>
            </div>
            <Footer></Footer>
            </Fragment>
        )
    }
}

class Slide extends React.Component {
    render() {
        return (
            <div
            className="speech font" 
            id={'bubble'+this.props.tabIndex+' slide'+this.props.slideIndex}>
                <span dangerouslySetInnerHTML={{__html: this.props.text}} />
            </div>
        )
    }
}

class SlideBtn extends React.Component {
    changeSlide = () => this.props.onClick(this.props.direction)
    render() {
        return (
            <i 
            id={this.props.direction === 1 ? 'arrowRight' : 'arrowLeft'}
            className={this.props.direction === 1 ? 'arrow Right' : 'arrow Left'}
            style={!this.props.isActive ? {visibility: 'hidden'} : {visibility: 'visible'}}
            direction={this.props.direction}
            onClick={ this.changeSlide } >
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                    <path style={{fill:'#FFF', opacity: 0.9}} className="d" d="M8.122 24l-4.122-4 8-8-8-8 4.122-4 11.878 12z"/>
                </svg>
            </i>
        )
    }
}

class Tab extends React.Component {
    changeTab = () => this.props.onClick(this.props.index)
    render() {
        return (
            <i 
            id={'btn'+this.props.index} 
            className={ this.props.isActive ? 'btn'+this.props.index+'On button' : 'btn'+this.props.index +' button' } 
            onClick={this.changeTab}>{ this.props.name }</i>
        )
    }
}

const Footer = () => {
    return (
        <footer>
            <a href="/CV Clément Jouffre fev19.pdf" className="font" style={{ textAlign: "left" }}>lien vers version papier</a>
            <a href="/Portfolio" className="font">portfolio</a>
            <div className="font">html, css, js</div>
            <div className="font" style={{ textAlign: "right" }}>ClmJ</div>
        </footer>
    );
};

export default App;
