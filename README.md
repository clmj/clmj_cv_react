# Clmj CV React

* CV Clément Jouffre React
* graphic assets & style.css © clément jouffre

## Installation

### `npm install`

## Déploiement local

### `npm start`

## Build

### `npm run build`

## Bugs

Pour une raison encore inconnue, les keyframes CSS d'animation de la bouche ne fonctionnent pas dans cette version.
